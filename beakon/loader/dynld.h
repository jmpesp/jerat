#ifndef DYNLD_H
#define DYNLD_H
#include <windef.h>
typedef struct {
	CHAR data[12];
} STRING12;

typedef HMODULE WINAPI (*PFN_LoadLibraryA)(
		LPCSTR lpFileName
		);
typedef FARPROC WINAPI (*PFN_GetProcAddress)(
		HMODULE hModule,
		LPCSTR  lpProcName
		);

LPVOID FindInKernel32(
		STRING12 sFunction
		);

extern PFN_LoadLibraryA Find_LoadLibraryA();

extern PFN_GetProcAddress Find_GetProcAddress();
#endif /* DYNLD_H */
