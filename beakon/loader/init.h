#ifndef INIT_H
#define INIT_H
#include "dynld.h"
#include <winsock2.h>
typedef int (*PFN_WSAStartup)(
		WORD      wVersionRequired,
		LPWSADATA lpWSAData
		);

typedef SOCKET (WSAAPI *PFN_WSASocketA)(
		int                 af,
		int                 type,
		int                 protocol,
		LPWSAPROTOCOL_INFOA lpProtocolInfo,
		GROUP               g,
		DWORD               dwFlags
		);

typedef int (WSAAPI *PFN_WSAConnect)(
		SOCKET				s,
		const struct sockaddr *name,
		int 				namelen,
		LPWSABUF 			lpCallerData,
		LPWSABUF			lpCalleeData,
		LPQOS				lpSQOS,
		LPQOS				lpGQOS
		);

typedef int (WSAAPI *PFN_WSARecv)(
		SOCKET                             s,
		LPWSABUF                           lpBuffers,
		DWORD                              dwBufferCount,
		LPDWORD                            lpNumberOfBytesRecvd,
		LPDWORD                            lpFlags,
		LPWSAOVERLAPPED                    lpOverlapped,
		LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
		);

typedef int (*PFN_WSAGetLastError)(

		);

typedef struct __FUNCTABLE {
	PFN_LoadLibraryA	pfnLoadLibraryA;
	PFN_GetProcAddress	pfnGetProcAddress;
	PFN_WSAStartup  	pfnWSAStartup;
	PFN_WSASocketA  	pfnWSASocketA;
	PFN_WSAConnect		pfnWSAConnect;
	PFN_WSARecv     	pfnWSARecv;
} FUNCTABLE, *LPFUNCTABLE;

DWORD InitFuncTable(LPFUNCTABLE pFuncTable);

#endif /* INIT_H */

