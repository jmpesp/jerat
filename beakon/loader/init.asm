;nasm -felf init.asm && i686-w64-mingw32-gcc init.o
struc PUBRES
	.pfnLoadLibraryA    resd 1
	.pfnGetProcAddress  resd 1
	.pfnWSAStartup      resd 1
	.pfnWSASocketA      resd 1
	.pfnWSAConnect      resd 1
	.pfnWSARecv         resd 1
endstruc
global _InitFuncTable
extern _Find_LoadLibraryA
extern _Find_GetProcAddress

section .text
_InitFuncTable: ; ebp *
	push ebp
	mov ebp, esp
	call pusheip
strtab:
	sWs2_32_dll db "Ws2_32.dll", 0
	sWSAStartup db "WSAStartup", 0
	sWSASocketA db "WSASocketA", 0
	sWSAConnect db "WSAConnect", 0
	sWSARecv    db "WSARecv", 0
initfunc: ;(al = stroffset, bl = resoffset)
	movzx eax, al
	movzx ebx, bl

	mov ecx, [ebp - 4]
	add ecx, eax ;edx = pRes->xx
	push ecx
	
	mov ecx, [ebp - 12]
	;mov [esp], ecx ;hModule
	push ecx
	
	mov edx, [ebp - 8]
	call edx
	
	mov ecx, [ebp + 8]
	mov [ecx + ebx], eax

	ret

pusheip:
	call _Find_GetProcAddress
	test eax, eax
	jz failed

	push eax ;push pGetProcAddress
	mov ebx, [ebp + 8]
	mov [ebx + PUBRES.pfnGetProcAddress], eax
	; pRes->pfnGetProcAddress = pGetProcAddress

	call _Find_LoadLibraryA
	; eax = pLoadLibraryA
	mov ebx, [ebp + 8]
	mov [ebx + PUBRES.pfnLoadLibraryA], eax
	; pRes->pfnLoadLibraryA = pLoadLibraryA

	
	mov edx, [ebp - 4]
	lea ebx, [edx + sWs2_32_dll - strtab]
	push ebx 
	call eax; LoadLibrary
	;add esp, 4
	push eax ;push hModule
	
	;mov edi, initfunc

	mov al, sWSAStartup - strtab
	mov bl, PUBRES.pfnWSAStartup
	call initfunc

	mov al, sWSASocketA - strtab
	mov bl, PUBRES.pfnWSASocketA
	call initfunc

	mov al, sWSAConnect - strtab
	mov bl, PUBRES.pfnWSAConnect
	call initfunc

	mov al, sWSARecv - strtab
	mov bl, PUBRES.pfnWSARecv
	call initfunc

	xor eax, eax
	jmp succeed
failed:
	mov eax, 1
succeed:
	mov esp, ebp
	pop ebp
	ret

