#! /usr/bin/env perl6
use v6;
react {
	whenever IO::Socket::Async.listen('0.0.0.0', 3333) -> $conn {
		whenever $conn.Supply.lines -> $line {
			$conn.print: qq:heredoc/END/; 
			Hello World!
			END
			#$conn.close;
			say $line;
		}
	}
	CATCH {
		default {
			say .^name, ': ', .Str;
			say "handled in $?LINE";
		}
	}
}
