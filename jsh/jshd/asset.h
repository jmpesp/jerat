#ifndef ASSET_H
#define ASSET_H
#include <windef.h>
struct ORIGINPTR;
/*typedef struct ORIGINPTR *LPASSET;*/
typedef void(*LPASSET)();
extern void s_Ws2_32_dll();
extern void s_WSAStartup();
extern void s_WSASocketA();
extern void s_WSAConnect();
extern void s_WSARecv();
extern LPVOID RelocateAsset(LPASSET lpAsset);
#endif
