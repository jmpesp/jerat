#ifndef INIT_H
#define INIT_H
#include "dynld.h"
#include <winsock2.h>
typedef int (*PFN_WSAStartup)(
		WORD      wVersionRequired,
		LPWSADATA lpWSAData
		);

typedef SOCKET (WSAAPI *PFN_WSASocketA)(
		int                 af,
		int                 type,
		int                 protocol,
		LPWSAPROTOCOL_INFOA lpProtocolInfo,
		GROUP               g,
		DWORD               dwFlags
		);

typedef int (WSAAPI *PFN_WSAConnect)(
		SOCKET				s,
		const struct sockaddr *name,
		int 				namelen,
		LPWSABUF 			lpCallerData,
		LPWSABUF			lpCalleeData,
		LPQOS				lpSQOS,
		LPQOS				lpGQOS
		);

typedef int (WSAAPI *PFN_WSARecv)(
		SOCKET                             s,
		LPWSABUF                           lpBuffers,
		DWORD                              dwBufferCount,
		LPDWORD                            lpNumberOfBytesRecvd,
		LPDWORD                            lpFlags,
		LPWSAOVERLAPPED                    lpOverlapped,
		LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
		);

typedef int (*PFN_WSAGetLastError)(

		);

typedef struct __FUNCTABLE {
	PFN_LoadLibraryA	fnLoadLibraryA;
	PFN_GetProcAddress	fnGetProcAddress;
	PFN_WSAStartup  	fnWSAStartup;
	PFN_WSASocketA  	fnWSASocketA;
	PFN_WSAConnect		fnWSAConnect;
	PFN_WSARecv     	fnWSARecv;
} FUNCTABLE, *LPFUNCTABLE;

DWORD InitFuncTable(LPFUNCTABLE pFuncTable);

#endif /* INIT_H */

