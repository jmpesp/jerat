#include "core.h"
#include "dynld.h"


#include "dynld.h"
#include "asset.h"
struct corectx {
	struct func {
		LoadLibraryA_f LoadLibrary;
		GetProcAddress_f GetProcAddress;
	};
};

int core_init(context_t *ctx)
{
	struct corectx **core = (struct corectx **)ctx->entry + 0;
	LoadLibraryA_f fnLoadLibraryA = (LoadLibraryA_f)Find_LoadLibraryA();
	GetProcAddress_f fnGetProcAddress = (GetProcAddress_f)Find_GetProcAddress();
	
	return 0;
}

context_t *getctx()
{
	return 0;
}

int geterror()
{
	return 0;
}


dl_t dlopen(const char* filename)
{
	dl_t dl;
	return dl;
}

void *dlsym(dl_t handle, char *symbol)
{
	return NULL;
}

void dlclose(dl_t handle)
{

}

void *malloc(size_t size)
{
	return NULL;
}

void *remalloc(void *ptr, size_t size)
{
	return NULL;
}

void free(void* ptr)
{

}

int exec(const char *pathname, const char *arg)
{
	return 0;
}

file_t *fopen(const char *filename)
{
	return NULL;
}

file_t *popen(const char *cmdline)
{
	return NULL;
}

file_t *topen(const char *address)
{
	return NULL;
}

void fclose(file_t *f)
{
}

int fprintf(file_t *f, char *fmt, ...)
{
	return 0;
}

int fputs(file_t *f, char *s)
{
	return 0;
}

int fgetc(file_t *f)
{
	return 0;
}

int fputc(file_t *f, char c)
{
	return 0;
}

size_t fread(file_t *f, void *ptr, size_t size)
{
	return 0;
}

int fwrite(file_t *f, void *ptr, size_t size)
{
	return 0;
}

int ftrunc(file_t *f)
{
	return 0;
}

int fseek(file_t *f, size_t offset)
{
	return 0;
}


