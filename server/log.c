#include "log.h"
#include <stdio.h>
#include <stdarg.h>
void Log(char *fmt, ...) {
	FILE *	f;
	va_list	vl;

	f = stdout;//fopen("c:\\testsvc.log","a+");
	if(f == NULL) return;

	va_start(vl,fmt);
	vfprintf(f, fmt, vl);
	va_end(vl);

	if(fileno(f)>2) fclose(f);
}


