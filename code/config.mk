CC = i686-w64-mingw32-gcc
AS = nasm
LINK = ld
AR = i686-w64-mingw32-gcc-ar

ASFLAGS = -fwin -Ox
CFLAGS = -Wall  -fno-builtin -Wno-builtin-declaration-mismatch -ansi -Os -m32 -I$(ROOT)/code
LDFLAGS = -e _start
